from rest_framework import serializers
from .models import Member
from django.contrib.auth.models import User

class MemberSerializer(serializers.ModelSerializer):
        class Meta:
                model = Member
                fields = ("name", "phone")

class UserSerializer(serializers.ModelSerializer):
        member = MemberSerializer(required=True)
        class Meta:
                model = User
                fields = ("username", "email", "password", "member")
                extra_kwargs = {"email" : {"required": True}}

        def create(self, validated_data):
                member = validated_data.pop("member")
                
                user = User.objects.create_user(**validated_data)



                member = Member.objects.create(
                        user = user,
                        status_id = 1,
                        name = member["name"],
                        phone = member["phone"],
                )
                member.save()
                return user

        def validate(self, attrs):
                email = attrs.get('email', '')
                if User.objects.filter(email=email).exists():
                        raise serializers.ValidationError(
                        {'email': ('A user with that email already exists.')})
                member = attrs.get('member', '')
                if member or len(member) == 0:
                        name = member.get('name', '')
                        if not name :
                                raise serializers.ValidationError(
                                {'member' : {'name': ('This field is required.')}})
                        phone = member.get('phone', '')
                        if not phone :
                                raise serializers.ValidationError(
                                {'member' : {'phone': ('This field is required.')}})
                return super().validate(attrs)