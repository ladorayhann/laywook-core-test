from django.db import models

class Status(models.Model):
    # Status Code is ID
    # 1 = unverified
    # 2 = pending
    # 3 = rejected
    # 4 = verified
    # 5 = inactive
    # 6 = leave
    status = models.CharField(max_length=20)

