from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from .status import Status
import uuid

class Member(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=50, null=True)
    phone = models.CharField(max_length=20, null=True)
    nik = models.CharField(max_length=30, blank=True, null=True)
    ktp = models.ImageField(blank=True, null=True)
    status = models.ForeignKey(Status, on_delete=models.CASCADE)
    nominal_pokok = models.IntegerField(blank=True, null=True)
    nominal_wajib = models.IntegerField(blank=True, null=True)
    nominal_sukarela = models.IntegerField(blank=True, null=True)
    date_created = models.DateField(default=timezone.now)
    date_updated = models.DateField(default=timezone.now)
    date_verified = models.DateField(blank=True, null=True)

