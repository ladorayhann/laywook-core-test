from django.contrib import admin
from .models import Member, Status

admin.site.register(Member)
admin.site.register(Status)