from django.shortcuts import render
from auths.serializers import MemberSerializer, UserSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.conf import settings
from django.contrib import auth
import jwt

@api_view(['POST'])
def login(request):
    data = request.data
    username = data.get('username', '')
    password = data.get('password', '')
    if username == '' and password != '':
        return Response({"message":"Username required"}, status=status.HTTP_400_BAD_REQUEST)
    elif password == '' and username != '':
        return Response({"message":"Password required"}, status=status.HTTP_400_BAD_REQUEST)
    elif password == '' and username == '':
        return Response({"message":"Username & Password required"}, status=status.HTTP_400_BAD_REQUEST)

    user = auth.authenticate(username=username, password=password)

    if user:
        auth_token = jwt.encode({"username":user.username}, settings.JWT_SECRET_KEY)

        serializers=UserSerializer(user)

        data = {"message": "success",
            "user": serializers.data, "token":auth_token
        }
        return Response(data, status=status.HTTP_200_OK )


    return Response({"message":"Invalid Credentials"}, status=status.HTTP_401_UNAUTHORIZED)
