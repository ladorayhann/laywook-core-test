from auths.tests.setup import TestSetUp

class TestViews(TestSetUp):

    def test_user_invalid_signup_request_no_member(self):

        res=self.client.post(self.register_url, {
            'username' : 'email',
            'email' : 'email@email.com',
            'password' : 'email'
        })
        self.assertEqual(res.data['member'][0], "This field is required.")
        self.assertEqual(res.status_code, 400)
    
    def test_user_invalid_signup_request_email_already_exist(self):
        res = self.client.post(self.register_url, self.user_data, format='json')
        res = self.client.post(self.register_url, {
            'username' : 'email1',
            'email' : 'email@email.com',
            'password' : 'email',
            'member' : {
                'name': 'member',
                'phone': '085716356765'
            }

        }, format='json')
        self.assertEqual(res.data['email'][0], "A user with that email already exists.")
        self.assertEqual(res.status_code, 400)

    def test_user_invalid_signup_request_no_name(self):
        res=self.client.post(self.register_url, {
            'username' : 'email1',
            'email' : 'email1@email.com',
            'password' : 'email',
            'member' : {
                'phone': '085716356765'
            }

        }, format='json')
        self.assertEqual(res.data['member']['name'], "This field is required.")
        self.assertEqual(res.status_code, 400)

    def test_user_invalid_signup_request_no_phone(self):
        res=self.client.post(self.register_url, {
            'username' : 'email1',
            'email' : 'email1@email.com',
            'password' : 'email',
            'member' : {
                'name': 'member',
            }

        }, format='json')
        self.assertEqual(res.data['member']['phone'], "This field is required.")
        self.assertEqual(res.status_code, 400)
        
    def test_user_cannot_login_with_no_data(self):
        res=self.client.post(self.login_url)
        self.assertEqual(res.data['message'], "Username & Password required")
        self.assertEqual(res.status_code, 400)

    def test_user_cannot_login_with_only_username(self):
        res=self.client.post(self.login_url, {'password':'password'})
        self.assertEqual(res.data['message'], "Username required")
        self.assertEqual(res.status_code, 400)
    
    def test_user_cannot_login_with_only_password(self):
        res=self.client.post(self.login_url, {'username':'username'})
        self.assertEqual(res.data['message'], "Password required")
        self.assertEqual(res.status_code, 400)

    def test_user_cannot_login_no_register(self):
        res=self.client.post(self.login_url, {'username':'username', 'password':'password'})
        self.assertEqual(res.data['message'], "Invalid Credentials")
        self.assertEqual(res.status_code, 401)

    def test_user_can_login(self):
        register = self.client.post(self.register_url, self.user_data, format='json')
        login=self.client.post(self.login_url, {'username':self.user_data['username'], 'password':self.user_data['password']})
        self.assertEqual(login.data['message'], "success")
        self.assertEqual(login.status_code, 200)
