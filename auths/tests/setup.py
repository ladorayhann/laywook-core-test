from rest_framework.test import APITestCase
from django.urls import reverse
from auths.models import Status

class TestSetUp(APITestCase):

    def setUp(self):

        self.register_url= reverse('signup')
        self.login_url= reverse('login')
        Status.objects.create(id=1, status="unverified").save()


        self.user_data = {
            'email' : 'email@email.com',
            'username' : 'email',
            'password' : 'email',
            'member' : {
                'name': 'member',
                'phone': '085716356765'
            }
        }

        return super().setUp()

    def tearDown(self):
        return super().tearDown()

