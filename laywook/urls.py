from django.contrib import admin
from django.urls import path, include

api_urlpatterns = [
    path('auth/', include('auths.urls')),
]

urlpatterns = [
    path('', admin.site.urls),
    path('api/', include(api_urlpatterns))
]
